/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fecha;

/**
 *
 * @author nachorod
 */
public class Fecha {

    private int dia;
    private int mes;
    private int año;

    public Fecha(int dia, int mes, int año) {
       this.dia=dia;
       this.mes=mes;
       this.año=año;
    }
    
    public Fecha(String fecha) {
        //Las fechas se pueden pasar separadas por / o -
        int pos1, pos2;
        pos1=fecha.indexOf("/");
        if (pos1!=-1) {
            this.dia=Integer.parseInt(fecha.substring(0, pos1));
            pos2=fecha.indexOf("/", pos1+1);
            if (pos2!=-1) {
                this.mes=Integer.parseInt(fecha.substring(pos1+1,pos2));
                this.año=Integer.parseInt(fecha.substring(pos2+1));
            }
        } else {
            pos1=fecha.indexOf("-");
            if (pos1!=-1) {
                this.dia=Integer.parseInt(fecha.substring(0, pos1));
                pos2=fecha.indexOf("-", pos1+1);
                if (pos2!=-1) {
                    this.mes=Integer.parseInt(fecha.substring(pos1+1,pos2));
                    this.año=Integer.parseInt(fecha.substring(pos2+1));
                }
            }            
        }
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        this.año = año;
    }
    
    public boolean esBisiesto() {
        return (año%4==0 && !(año%100==0)) || año%400==0;
    }
    
    // Devuelve una cadena con el número de años y días
    // transcurridos desde la fecha inicial a la final.
    public String restarFecha(Fecha fin) {
        String resultado="";
        int añoFin=fin.getAño();
        int mesFin=fin.getMes();
        int diaFin=fin.getDia();
        int años;
        int dias;
        Fecha fechaAux;
        if (mesFin>this.mes || (mesFin==this.mes && diaFin>this.dia)) {
            años=añoFin-this.año;
            fechaAux=new Fecha(fin.getDia(), fin.getMes(), this.año);
            dias=getDiasHasta(fin);
            resultado=años + " años " + dias + " dias ";
        } else {
            años=añoFin-this.año-1;
            fechaAux=new Fecha(fin.getDia(), fin.getMes(), this.año);
            if (fin.esBisiesto()) {
                dias=366-getDiasDesde(fechaAux);
            } else {
                dias=365-getDiasDesde(fechaAux);
            }
            resultado=años + " años " + dias + " dias";
        }
        
        return resultado;
    }
    
    // Devuelve el número de días desde la fecha inicial
    // dentro del mismo año. Si la fechaInicial es mayor
    // o igual que la fecha, devuelve cero. O si el año
    // es distinto también devuelve cero.
    public int getDiasDesde(Fecha fechaInicial) {
        int resultado=0;
        if (this.año==fechaInicial.getAño()) {
            if (this.mes==fechaInicial.getMes()) {
                if (this.dia>fechaInicial.getDia()) {
                    resultado=fechaInicial.getDia()-this.dia;
                }
            } else if (this.mes>fechaInicial.getMes()) {
                int diasUltimoMes=this.dia;
                int PrimerMes=fechaInicial.getMes();
                int diaFechaInicial=fechaInicial.getDia();
                int diasPrimerMes=diasMes(PrimerMes,this.año)-diaFechaInicial;
                int diasMesesCompletos=0;
                for (int m=fechaInicial.getMes()+1; m<this.mes;m++) {
                    diasMesesCompletos+=diasMes(m,this.año);
                }
                resultado=diasPrimerMes+diasUltimoMes+diasMesesCompletos;
            }
        }
        return resultado;
    }

    // Devuelve el número de días desde la fecha hasta la
    // fecha final pasada como parámetro dentro del mismo
    // año.
    public int getDiasHasta(Fecha fechaFinal) {
        int resultado=0;
        if (fechaFinal.getAño()==this.año) {
            if (fechaFinal.getMes()==this.mes) {
                if (fechaFinal.getDia()>this.dia) {
                    resultado=fechaFinal.getDia()-this.dia;
                }
            } else if (fechaFinal.getMes()>this.mes) {
                int diasUltimoMes=fechaFinal.getDia();
                int diasPrimerMes=diasMes(this.mes,this.año)-this.dia;
                int diasMesesCompletos=0;
                for (int m=this.mes+1; m<fechaFinal.getMes();m++) {
                    diasMesesCompletos+=diasMes(m,this.año);
                }
                resultado=diasPrimerMes+diasUltimoMes+diasMesesCompletos;                
            }
        }
        return resultado;
    }
    
    // Método que devuelve el número de días restantes
    // para acabar el año actual.
    public int getDiasRestantes() {
        int dias=diasMes(this.mes,this.año)-this.dia;
        for (int i=this.mes+1; i<=12; i++) {
            dias+=diasMes(i,this.año);
        }
        return dias;
    }
    
    public int diasMes(int mes, int año) {
        int resultado=0;
        switch (mes) {
            case 1:
                resultado=31;
                break;
            case 2:
                if (esBisiesto()) {
                    resultado= 29;
                } else {
                    resultado= 28;
                }
                break;
            case 3:
                resultado=31;
                break;
            case 4:
                resultado= 30;
                break;
            case 5:
                resultado= 31;
                break;
            case 6:
                resultado= 30;
                break;
            case 7:
                resultado= 31;
                break;
            case 8:
                resultado= 31;
                break;
            case 9:
                resultado= 30;
                break;
            case 10:
                resultado= 31;
                break;
            case 11:
                resultado= 30;
                break;
            case 12:
                resultado= 31;
                break;
        }
        return resultado;
    }
    
    public void mostrar() {
        System.out.println(añadeCeros(dia,2) + "/" + añadeCeros(mes,2) + "/" + año);
    }
   
    static String añadeCeros(int num, int numCeros) {
        int cerosAñadir;
        String aux=num + "";
        cerosAñadir=numCeros-aux.length();
        for (int i=1; i<=cerosAñadir; i++) {
            aux="0" + aux;
        }
        return aux;
    }
}
