package fecha;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author nachorod
 */
public class FechaApp {
    public static void main(String[] args) {
        Fecha f1,f2, f3;
        f1=new Fecha("21-03-2017");
        f2=new Fecha("19/03/1971");
        f3=new Fecha(01,01,1971);
        f1.mostrar();
        f2.mostrar();
        f3.mostrar();
        System.out.println(f3.getDiasRestantes()+"");
        System.out.println(f2.restarFecha(f1));
    }
}
